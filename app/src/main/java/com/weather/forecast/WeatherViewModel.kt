package com.weather.forecast

import android.content.Context
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.weather.forecast.content.models.WeatherModel
import com.weather.forecast.content.ui.RefreshState
import com.weather.forecast.domain.WeatherInteractor
import com.weather.forecast.domain.models.Location

class WeatherViewModel: ViewModel() {

    private val _weatherLiveData = MutableLiveData<MutableList<WeatherModel>>()
    val weatherLiveData: LiveData<MutableList<WeatherModel>> = _weatherLiveData

    val refreshState = mutableStateOf(RefreshState.NOT_REFRESHING)

    private val interactor = WeatherInteractor()

    private var errorCallback: ((message: String) -> Unit)? = null

    fun error(callback: (message: String) -> Unit) {
        errorCallback = callback
    }

    fun fetch(context: Context) {
        if (refreshState.value != RefreshState.NOT_REFRESHING)
            return
        refreshState.value = RefreshState.REFRESHING_180
        interactor.getLocation(
            context, {
                interactor.getWeather(
                    it,
                    this::onLocation,
                    this::onError
                )
            },
            this::onError
        )
    }

    private fun onLocation(location: Location) {
        if (_weatherLiveData.value == null) {
            val weatherList = mutableListOf<WeatherModel>()
            val size = location.consolidatedWeather?.size ?: return
            for (i in 0 until size) {
                weatherList.add(WeatherModel.from(location, i))
            }
            _weatherLiveData.value = weatherList
        } else {
            val weatherList = _weatherLiveData.value ?: return
            val size = location.consolidatedWeather?.size ?: return
            for (i in 0 until size) {
                weatherList[i].applyChanges(location, i)
            }
        }
        refreshState.value = RefreshState.NOT_REFRESHING
    }

    private fun onError(message: String) {
        refreshState.value = RefreshState.NOT_REFRESHING
        errorCallback?.invoke(message)
    }
}