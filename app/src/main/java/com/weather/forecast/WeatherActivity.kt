package com.weather.forecast

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.unit.dp
import androidx.core.app.ActivityCompat
import com.weather.forecast.content.ui.*
import com.weather.forecast.utils.ui.WeatherforecastTheme

class WeatherActivity : AppCompatActivity() {

    private val locationPermission = 12345

    private val viewModel: WeatherViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
        window.backgroundIcon()
        viewModel.error { Toast.makeText(applicationContext, it, Toast.LENGTH_SHORT).show() }
        setContent {
            WeatherforecastTheme {
                viewModel.weatherLiveData.observeAsState().value?.let {
                    WeatherContent(
                        data = it
                    )
                }
                Stack(
                    modifier = Modifier
                        .fillMaxSize()
                ) {
                    RefreshIcon(
                        modifier = Modifier
                            .gravity(Alignment.TopEnd)
                            .padding(top = 50.dp),
                        refreshState = viewModel.refreshState,
                        click = this@WeatherActivity::fetch
                    )
                }
            }
        }
        fetch()
    }

    private fun fetch() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                locationPermission
            )
        } else
            viewModel.fetch(applicationContext)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == locationPermission) {
            viewModel.fetch(applicationContext)
        } else
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}
