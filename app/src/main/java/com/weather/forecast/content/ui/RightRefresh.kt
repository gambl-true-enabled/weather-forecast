package com.weather.forecast.content.ui

import androidx.compose.animation.animate
import androidx.compose.animation.core.FloatPropKey
import androidx.compose.animation.core.transitionDefinition
import androidx.compose.animation.core.tween
import androidx.compose.animation.transition
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawShadow
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.rotateRad
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.ui.tooling.preview.Preview
import com.weather.forecast.R

enum class RefreshState {
    REFRESHING_180,
    REFRESHING_360,
    NOT_REFRESHING
}

private val rotationKey = FloatPropKey()

private val refreshingTransitionDefinition = transitionDefinition<RefreshState> {
    state(RefreshState.REFRESHING_180) {
        this[rotationKey] = 180f
    }
    state(RefreshState.REFRESHING_360) {
        this[rotationKey] = 360f
    }
    state(RefreshState.NOT_REFRESHING) {
        this[rotationKey] = 0.0f
    }
    val rotateDuration = 1000
    transition(fromState = RefreshState.REFRESHING_180) {
        rotationKey using tween(
                durationMillis = rotateDuration
        )
    }
    transition(fromState = RefreshState.REFRESHING_360) {
        rotationKey using tween(
                durationMillis = 0
        )
    }
    transition(fromState = RefreshState.NOT_REFRESHING) {
        rotationKey using tween(
                durationMillis = rotateDuration
        )
    }
}

@Composable
fun RefreshIcon(
    modifier: Modifier = Modifier,
    refreshState: MutableState<RefreshState> = mutableStateOf(RefreshState.NOT_REFRESHING),
    click: () -> Unit = {}
) {
    val shape = RoundedCornerShape(topLeft = 24.dp, bottomLeft = 24.dp)
    val rotateImageAsset = imageResource(id = R.drawable.ic_refresh)
    val transition = transition(
            definition = refreshingTransitionDefinition,
            toState = refreshState.value
    )
    val animation = animate(
        target = transition[rotationKey],
        endListener = {
            when(refreshState.value) {
                RefreshState.REFRESHING_360 -> refreshState.value = RefreshState.REFRESHING_180
                RefreshState.REFRESHING_180 -> refreshState.value = RefreshState.REFRESHING_360
            }
        }
    )
    Surface(
            shape = shape,
            modifier = modifier
                    .clip(shape = shape)
                    .clickable(onClick = click),
            color = colorResource(id = R.color.grey_back)
    ) {
        Canvas(
            modifier = Modifier
                .width(88.dp)
                .height(48.dp)
                .padding(start = 16.dp, end = 40.dp, top = 8.dp, bottom = 8.dp),
            onDraw = {
                rotate(animation) {
                    drawImage(rotateImageAsset)
                }
            }
        )
    }
}
