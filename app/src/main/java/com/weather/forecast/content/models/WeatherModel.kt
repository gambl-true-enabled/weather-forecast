package com.weather.forecast.content.models

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import com.github.thunder413.datetimeutils.DateTimeUtils
import com.weather.forecast.R
import com.weather.forecast.domain.models.Location
import java.text.SimpleDateFormat
import java.util.*

data class WeatherModel(
    val cityName: MutableState<String> = mutableStateOf("-"),
    var weatherIcon: MutableState<Int> = mutableStateOf(-1),
    val temperatureInCelsius: MutableState<String> = mutableStateOf("-°С"),
    val dateName: MutableState<String> = mutableStateOf("-"),
    val dateFull: MutableState<String> = mutableStateOf("-")
) {

    private fun WeatherModel.set(model: Location, position: Int) {
        this@WeatherModel.cityName(model.title)
        model.consolidatedWeather?.get(position)?.let { weather ->
            this@WeatherModel.weatherIcon(weather.weatherStateAbbr)
            this@WeatherModel.temperature(weather.theTemp)
            this@WeatherModel.date(weather.applicableDate)
        }
    }

    companion object{
        fun from(model: Location, position: Int) = WeatherModel().apply {
            set(model, position)
        }
    }

    fun applyChanges(model: Location, position: Int) {
        set(model, position)
    }

    private fun cityName(name: String?) {
        cityName.value = name ?: ""
    }

    private fun weatherIcon(name: String?) {
        weatherIcon.value = when(name) {
            "sn" -> R.drawable.ic_snow
            "sl" -> R.drawable.ic_sleet
            "h" -> R.drawable.ic_hail
            "t" -> R.drawable.ic_thunderstorm
            "hr" -> R.drawable.ic_heavy_rain
            "lr" -> R.drawable.ic_light_rain
            "s" -> R.drawable.ic_showers
            "hc" -> R.drawable.ic_heavy_cloud
            "lc" -> R.drawable.ic_light_cloud
            "c" -> R.drawable.ic_clear
            else -> R.drawable.ic_clear
        }
    }

    private fun temperature(centigrade: Double?) {
        temperatureInCelsius.value = (centigrade?.toInt() ?: 0).toString() + "°С"
    }

    private fun date(string: String?) {
        string ?: return
        val date = SimpleDateFormat("yyyy-MM-dd").parse(string) ?: return
        dateFull.value = SimpleDateFormat("dd.MM.yyyy").format(date)
        dateName.value = DateTimeUtils.formatWithPattern(string, "EEEE", Locale.ENGLISH)
    }
}