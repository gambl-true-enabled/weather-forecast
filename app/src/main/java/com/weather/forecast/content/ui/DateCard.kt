package com.weather.forecast.content.ui

import androidx.compose.foundation.ProvideTextStyle
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope.gravity
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.unit.dp
import com.weather.forecast.R
import com.weather.forecast.utils.ui.typography

@Composable
fun DateCard(
        modifier: Modifier = Modifier,
        dayOfWeek: State<String> = mutableStateOf(""),
        fullDate: State<String> = mutableStateOf("")
) {
    val shape = RoundedCornerShape(24.dp)
    val itemsModifier = Modifier
            .gravity(Alignment.CenterHorizontally)
    Surface(
            modifier = modifier
                    .clip(shape = shape),
            color = colorResource(id = R.color.grey_back),
            shape = shape
    ) {
        Column(
                modifier = Modifier
                        .width(214.dp)
                        .padding(12.dp)
        ) {
            ProvideTextStyle(value = typography.h2) {
                Text(
                        text = dayOfWeek.value,
                        modifier = itemsModifier
                )
            }
            ProvideTextStyle(value = typography.h3) {
                Text(
                        text = fullDate.value,
                        modifier = itemsModifier
                                .padding(bottom = 70.dp)
                )
            }
        }
    }
}