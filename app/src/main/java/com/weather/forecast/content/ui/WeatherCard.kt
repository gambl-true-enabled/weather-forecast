package com.weather.forecast.content.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.ProvideTextStyle
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope.gravity
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import com.weather.forecast.R
import com.weather.forecast.utils.ui.typography

@Composable
fun WeatherCard(
        weather: State<Int>,
        modifier: Modifier = Modifier,
        cityName: State<String> = mutableStateOf(""),
        temperature: State<String> = mutableStateOf("")
) {
    val shape = RoundedCornerShape(12.dp)
    val itemsModifier = Modifier
            .gravity(Alignment.CenterHorizontally)
    Surface(
            modifier = modifier
                    .clip(shape = shape),
            color = colorResource(id = R.color.grey_back),
            shape = shape
    ) {
        Column(
                modifier = Modifier
                        .padding(24.dp)
        ) {
            Image(
                    asset = vectorResource(id = weather.value),
                    modifier = itemsModifier
                            .padding(bottom = 8.dp)
            )
            ProvideTextStyle(value = typography.h4) {
                Text(
                        text = cityName.value,
                        modifier = itemsModifier
                )
            }
            ProvideTextStyle(value = typography.h1) {
                Text(
                        text = temperature.value,
                        modifier = itemsModifier
                )
            }
        }
    }
}