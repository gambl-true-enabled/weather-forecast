package com.weather.forecast.content.ui

import androidx.compose.foundation.ProvideTextStyle
import androidx.compose.foundation.Text
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.weather.forecast.R
import com.weather.forecast.utils.ui.typography

@Composable
fun ButtonWeather(
        modifier: Modifier = Modifier,
        text: State<String> = mutableStateOf(""),
        click: () -> Unit = {}
) {
    val shape = RoundedCornerShape(24.dp)
    Surface(
            modifier = modifier
                    .width(214.dp)
                    .clip(shape = shape)
                    .clickable(onClick = click),
            shape = shape,
            color = colorResource(id = R.color.button_color)
    ) {
        ProvideTextStyle(value = typography.h5) {
            Text(
                    text = text.value,
                    modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 16.dp),
                    textAlign = TextAlign.Center
            )
        }
    }
}
