package com.weather.forecast.content.ui

import android.util.Range
import android.view.Window
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.imageResource
import com.weather.forecast.R
import java.util.*

fun Window.backgroundIcon() {
    val hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
    val morning = Range(5, 11)
    val day = Range(12, 20)
    val night = Range(4, 21)
    val icon = when {
        morning.contains(hour) -> R.drawable.back_morning
        day.contains(hour) -> R.drawable.back_day
        night.contains(hour) -> R.drawable.back_night
        else -> R.drawable.back_day
    }
    setBackgroundDrawableResource(icon)
}