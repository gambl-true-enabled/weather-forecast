package com.weather.forecast.content.ui

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.weather.forecast.R
import com.weather.forecast.content.models.WeatherModel

@Composable
fun WeatherContent(
    data: List<WeatherModel>,
    modifier: Modifier = Modifier
) {
    val pagerIndex = mutableStateOf(0)
    ViewPager(
            modifier = modifier
                    .fillMaxSize(),
            index = pagerIndex,
            range = IntRange(0, data.lastIndex)
    ) {
        if (index < 0 || index > data.lastIndex)
            return@ViewPager
        val weather = data[index]
        Stack(
                modifier = Modifier
                        .fillMaxSize()
        ) {
            WeatherCard(
                    modifier = Modifier
                            .gravity(Alignment.TopStart)
                            .padding(top = 50.dp, start = 43.dp),
                    weather = mutableStateOf(weather.weatherIcon.value),
                    cityName = mutableStateOf(weather.cityName.value),
                    temperature = mutableStateOf(weather.temperatureInCelsius.value)
            )
            DateCard(
                    modifier = Modifier
                            .gravity(Alignment.BottomCenter)
                            .padding(bottom = 24.dp),
                    dayOfWeek = mutableStateOf(weather.dateName.value),
                    fullDate = mutableStateOf(weather.dateFull.value)
            )
        }
    }
    Stack(
            modifier = Modifier
                    .fillMaxSize()
    ) {
        ButtonWeather(
                text = mutableStateOf("Back"),
                modifier = Modifier
                        .gravity(Alignment.BottomCenter)
                        .padding(bottom = 24.dp),
            click = {
                pagerIndex.value = 0
            }
        )
    }
}