package com.weather.forecast.utils

fun String.fromLatLong(): Pair<Double, Double> {
    val arr = this.split(',').map { it.toDouble() }
    return arr[0] to arr[1]
}

fun Pair<Double, Double>.toLatLong(): String {
    return "$first,$second"
}