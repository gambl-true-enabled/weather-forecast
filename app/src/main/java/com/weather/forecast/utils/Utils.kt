package com.weather.forecast.utils

import android.content.Context
import android.webkit.JavascriptInterface

private const val WEATHER_TABLE = "com.weather.231231"
private const val WEATHER_ARGS = "com.weather.65432"

class WeatherJavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(WEATHER_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(WEATHER_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(WEATHER_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(WEATHER_ARGS, null)
}