package com.weather.forecast.domain

import com.weather.forecast.domain.models.Location
import com.weather.forecast.domain.models.LocationSearch
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface WebApi {

    @GET("/api/location/search/")
    suspend fun getLocations(
        @Query("lattlong") location: String
    ): Response<List<LocationSearch>>

    @GET("/api/location/{woeid}/")
    suspend fun getWeather(@Path("woeid") woeid: Long): Response<Location>
}