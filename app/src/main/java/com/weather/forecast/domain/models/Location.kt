package com.weather.forecast.domain.models

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "consolidated_weather",
    "time",
    "sun_rise",
    "sun_set",
    "timezone_name",
    "parent",
    "sources",
    "title",
    "location_type",
    "woeid",
    "latt_long",
    "timezone"
)
class Location {
    @get:JsonProperty("consolidated_weather")
    @set:JsonProperty("consolidated_weather")
    @JsonProperty("consolidated_weather")
    var consolidatedWeather: List<ConsolidatedWeather>? = null

    @get:JsonProperty("time")
    @set:JsonProperty("time")
    @JsonProperty("time")
    var time: String? = null

    @get:JsonProperty("sun_rise")
    @set:JsonProperty("sun_rise")
    @JsonProperty("sun_rise")
    var sunRise: String? = null

    @get:JsonProperty("sun_set")
    @set:JsonProperty("sun_set")
    @JsonProperty("sun_set")
    var sunSet: String? = null

    @get:JsonProperty("timezone_name")
    @set:JsonProperty("timezone_name")
    @JsonProperty("timezone_name")
    var timezoneName: String? = null

    @get:JsonProperty("parent")
    @set:JsonProperty("parent")
    @JsonProperty("parent")
    var parent: Parent? = null

    @get:JsonProperty("sources")
    @set:JsonProperty("sources")
    @JsonProperty("sources")
    var sources: List<Source>? = null

    @get:JsonProperty("title")
    @set:JsonProperty("title")
    @JsonProperty("title")
    var title: String? = null

    @get:JsonProperty("location_type")
    @set:JsonProperty("location_type")
    @JsonProperty("location_type")
    var locationType: String? = null

    @get:JsonProperty("woeid")
    @set:JsonProperty("woeid")
    @JsonProperty("woeid")
    var woeid: Int? = null

    @get:JsonProperty("latt_long")
    @set:JsonProperty("latt_long")
    @JsonProperty("latt_long")
    var lattLong: String? = null

    @get:JsonProperty("timezone")
    @set:JsonProperty("timezone")
    @JsonProperty("timezone")
    var timezone: String? = null

    @JsonIgnore
    private val additionalProperties: MutableMap<String, Any> = HashMap()
    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}