package com.weather.forecast.domain.models

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder("title", "location_type", "woeid", "latt_long")
class Parent {
    @get:JsonProperty("title")
    @set:JsonProperty("title")
    @JsonProperty("title")
    var title: String? = null

    @get:JsonProperty("location_type")
    @set:JsonProperty("location_type")
    @JsonProperty("location_type")
    var locationType: String? = null

    @get:JsonProperty("woeid")
    @set:JsonProperty("woeid")
    @JsonProperty("woeid")
    var woeid: Int? = null

    @get:JsonProperty("latt_long")
    @set:JsonProperty("latt_long")
    @JsonProperty("latt_long")
    var lattLong: String? = null

    @JsonIgnore
    private val additionalProperties: MutableMap<String, Any> = HashMap()
    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}