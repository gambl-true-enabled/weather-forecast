package com.weather.forecast.domain.models

import com.fasterxml.jackson.annotation.*
import java.util.*

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(
    "id",
    "weather_state_name",
    "weather_state_abbr",
    "wind_direction_compass",
    "created",
    "applicable_date",
    "min_temp",
    "max_temp",
    "the_temp",
    "wind_speed",
    "wind_direction",
    "air_pressure",
    "humidity",
    "visibility",
    "predictability"
)
class ConsolidatedWeather {
    @get:JsonProperty("id")
    @set:JsonProperty("id")
    @JsonProperty("id")
    var id: Long? = null

    @get:JsonProperty("weather_state_name")
    @set:JsonProperty("weather_state_name")
    @JsonProperty("weather_state_name")
    var weatherStateName: String? = null

    @get:JsonProperty("weather_state_abbr")
    @set:JsonProperty("weather_state_abbr")
    @JsonProperty("weather_state_abbr")
    var weatherStateAbbr: String? = null

    @get:JsonProperty("wind_direction_compass")
    @set:JsonProperty("wind_direction_compass")
    @JsonProperty("wind_direction_compass")
    var windDirectionCompass: String? = null

    @get:JsonProperty("created")
    @set:JsonProperty("created")
    @JsonProperty("created")
    var created: String? = null

    @get:JsonProperty("applicable_date")
    @set:JsonProperty("applicable_date")
    @JsonProperty("applicable_date")
    var applicableDate: String? = null

    @get:JsonProperty("min_temp")
    @set:JsonProperty("min_temp")
    @JsonProperty("min_temp")
    var minTemp: Double? = null

    @get:JsonProperty("max_temp")
    @set:JsonProperty("max_temp")
    @JsonProperty("max_temp")
    var maxTemp: Double? = null

    @get:JsonProperty("the_temp")
    @set:JsonProperty("the_temp")
    @JsonProperty("the_temp")
    var theTemp: Double? = null

    @get:JsonProperty("wind_speed")
    @set:JsonProperty("wind_speed")
    @JsonProperty("wind_speed")
    var windSpeed: Double? = null

    @get:JsonProperty("wind_direction")
    @set:JsonProperty("wind_direction")
    @JsonProperty("wind_direction")
    var windDirection: Double? = null

    @get:JsonProperty("air_pressure")
    @set:JsonProperty("air_pressure")
    @JsonProperty("air_pressure")
    var airPressure: Double? = null

    @get:JsonProperty("humidity")
    @set:JsonProperty("humidity")
    @JsonProperty("humidity")
    var humidity: Int? = null

    @get:JsonProperty("visibility")
    @set:JsonProperty("visibility")
    @JsonProperty("visibility")
    var visibility: Double? = null

    @get:JsonProperty("predictability")
    @set:JsonProperty("predictability")
    @JsonProperty("predictability")
    var predictability: Int? = null

    @JsonIgnore
    private val additionalProperties: MutableMap<String, Any> = HashMap()
    @JsonAnyGetter
    fun getAdditionalProperties(): Map<String, Any> {
        return additionalProperties
    }

    @JsonAnySetter
    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}