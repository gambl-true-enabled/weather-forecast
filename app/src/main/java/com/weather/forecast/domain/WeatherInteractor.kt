package com.weather.forecast.domain

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.location.LocationManager
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import com.weather.forecast.domain.models.Location
import com.weather.forecast.domain.models.LocationSearch
import com.weather.forecast.utils.toLatLong
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.Exception

class WeatherInteractor {

    private val webApi: WebApi = NetworkProxyGetter.retrofit.create(WebApi::class.java)

    @Throws(Exception::class)
    private suspend fun getLocations(
        location: String,
        callback: suspend (data: List<LocationSearch>) -> Unit,
        error: (message: String) -> Unit
    ) {
        val response = webApi.getLocations(location)
        val data = response.body()
        if (response.isSuccessful && data != null) {
            callback(data)
        } else {
            withContext(Dispatchers.Main) {
                error("Error fetch locations")
            }
        }
    }

    fun getLocation(
        context: Context,
        callback: (location: Pair<Double, Double>) -> Unit,
        error: (message: String) -> Unit
    ) {
        context.apply {
            CoroutineScope(Dispatchers.IO).launch {
                val service = LocationServices.getFusedLocationProviderClient(this@apply)
                if (ActivityCompat.checkSelfPermission(
                        this@apply,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this@apply,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    service.lastLocation.addOnSuccessListener { location ->
                        if (location != null) {
                            callback(location.latitude to location.longitude)
                        } else {
                            callback(36.96 to -122.02)
                            error("No location found")
                        }
                    }
                } else
                    callback(36.96 to -122.02)
            }
        }
    }

    fun getWeather(
        location: Pair<Double, Double>,
        callback: (data: Location) -> Unit,
        error: (message: String) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                getLocations(location.toLatLong(), {
                    getWeather(it, callback, error)
                }, error)
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    error("No internet connection")
                }
            }
        }
    }

    private suspend fun getWeather(
        locations: List<LocationSearch>,
        callback: (data: Location) -> Unit,
        error: (message: String) -> Unit
    ) {
        val woeid = locations.first().woeid
        if (woeid != null) {
            val response = webApi.getWeather(woeid)
            val data = response.body()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful && data != null) {
                    callback(data)
                } else {
                    error("Error fetch location weather")
                }
            }
        } else {
            withContext(Dispatchers.Main) {
                error("No woeid found")
            }
        }
    }
}